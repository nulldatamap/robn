ifeq ($(OS),Windows_NT)
	SEP := ;
else
	SEP := :
endif

LIBS:=".$(SEP)./src$(SEP)libs/nanopass$(SEP)libs/thunderchez"
SCHEME ?=scheme
SCHEME_OPTS=--libdirs $(LIBS) --compile-imported-libraries
SS=$(SCHEME) $(SCHEME_OPTS)

.PHONY: tests clean

tests: src/test/all-tests.ss src/test/parser-tests.ss src/test/compiler-tests.ss
	@$(SS) --script src/test/all-tests.ss

clean:
	@find . -name \*.so | xargs rm -rf
