#!chezscheme
(library (compiler passes)
  (export token-s-exp->Lsrc
          coecere-symbols-to-qs
          make-variables-unique
          make-do-explicit
          check-attributes
          build-module
          flatten-dos
          make-variables-unique
          annotate-types)
  (import (chezscheme)
          (syntax token)
          (compiler core)
          (compiler modules)
          (compiler languages)
          (compiler types)
          (compiler pipeline)
          (util misc)
          (util arrows)
          (util graph-algorithms)
          (nanopass)
          (matchable)
          (only (srfi s69 basic-hash-tables)
                hash-table->alist
                hash-table-fold
                hash-table-keys
                hash-table-ref)
          (only (srfi s1 lists)
                every last concatenate list-index any delete delete-duplicates
                count unzip2 lset-difference zip lset-union lset-xor lset=
                take-while drop-while drop-right last))

  ;; --------------------------------- Pre-passes -------------------------------

  ;; Go through the token s-exp, and each list that starts with a symbol token
  ;; which matches any of the language construction symbols will be transformed
  ;; into the (<symbol> <location> rest ...) syntax that Lsrc expects, the rest
  ;; is recursive tranformed.
  (define (token-s-exp->Lsrc src)
    (define syntax-errors '())

    (define (syntax-error s-exp)
      (push-set! syntax-errors
                 (make-compilation-error #f (list 'invalid-syntax
                                                  (car s-exp)
                                                  s-exp)))
      #f)

    (define (pat? s-exp)
      (match s-exp
        [((? unqualified-symbol?) ((? ident?) (? ident?)) ...) #t]
        [((? unqualified-symbol?) (? ident?)) #t]
        [((? unqualified-symbol?)) #t]
        [(? qualified-symbol-token?) #t]
        [else #f]))

    (define (valid-tyvar? x)
      (match x
        [(($ token 'ident "'") (? ident?)) #t]
        [:_ #f]))

    (define (valid-variant? vrns)
      (match vrns
        [(? ident?) #t]
        [((? ident?) (? valid-type?) ...) #t]
        [:_ #f]))

    (define (valid-typeann? x)
      (match x
        [(($ token 'ident ":" ) (? valid-type?)) #t]
        [:_ #f]))

    (define (valid-type? x)
      (match x
        [(($ token 'ident "->") (? valid-type?) ..1) #t]
        [(($ token 'ident "->") :_ ...) #f]
        [($ token 'ident "unit") #t]
        [(? ident?) #t]
        [(? valid-tyvar?) #t]
        [((? ident?) (? valid-type?) ...) #t]
        [:_ #f]))

    (define (valid? s-exp)
      (or (match s-exp
            [('module (? unqualified-symbol?) :_ ...) #t]
            [('module :_ ...) #f]

            [('if cnd thn) #t]
            [('if cnd thn els) #t]
            [('if :_ ...) #f]

            [('def (? ident?) bdy) #t]
            [('def ((? ident?) ..1) bdy ...) #t]
            [('def :_ ...) #f]

            [('decl (? ident?) (? valid-type?)) #t]
            [('decl :_ ...) #f]

            [('let (((? ident?) val) ...) bdy ...) #t]
            [('let :_ ...) #f]

            [('lambda rest ...)
             (match (drop-while kw? rest)
               [(((? ident?) ...) bdy ...) #t]
               [:_ #f])]

            [('match subj ((? pat?) bdy ..1) ..1) #t]
            [('match :_ ...) #f]

            [('defstruct rest ...)
             (match (drop-while kw? rest)
               [((? ident?) ((? ident?) (? valid-type?)) ...) #t]
               [(((? ident?) (? valid-tyvar?) ...) ((? ident?) (? valid-type?)) ...) #t]
               [:_ #f])]

            [('deftype rest ...)
             (match (drop-while kw? rest)
               [((? ident?) (? valid-variant?) ...) #t]
               [(((? ident?) (? valid-tyvar?) ...) (? valid-variant?) ...) #t]
               [:_ #f])]

            [else #t])
          (syntax-error s-exp)))

    (define (transform-pat pat)
      (match pat
        [(ctr (x* fld*) ...) (append (list 'p-struct ctr) (zip x* fld*))]
        [(vrt mx ...) (cons 'p-sum pat)]
        [vrt (list 'p-sum vrt #f)]
        [else (errorf 'transform-pat "Unexpected pattern syntax: ~A\n" pat)]))

    (define (transform-type ty)
      (match ty
        [(head rest ...)
         (match head
           [($ token 'ident "->")
            (let ((rest* (map transform-type rest)))
              (list '->
                    (token-span head)
                    (drop-right rest* 1)
                    (last rest*)))]
           [($ token 'ident (? (lambda (x) (member (string->symbol x) type-constructs))))
            (append (list (string->symbol (token-value head))
                          (token-span head))
                    (map transform-type rest))]
           [:_ (map transform-type ty)])]
        [x x]))

    (define (transform s-exp)
      (match s-exp
        [(head rest ...)
         (match head
           ;; Validate and transform match as a language construct, but
           ;; also transform each arm's pattern into the Pattern non-terminal syntax
           ;; TODO: This should be generalized to all syntactic constructions that
           ;;       deal with patterns.
           [($ token 'ident "match" start end)
            (if (valid? (cons 'match rest))
                (match rest
                  [(x (pat* . bdy**) ...)
                   (append (list 'match
                                 (cons start end)
                                 (transform x))
                           (map (lambda (pat bdy*)
                                  (cons (transform-pat pat)
                                        (transform bdy*)))
                                pat*
                                bdy**))]
                  [else (errorf 'transform "Unexpected match syntax: ~A\n" s-exp)])
                'invalid)]
           [($ token 'ident "deftype" start end)
            (if (valid? (cons 'deftype rest))
                (let ((attrs (take-while kw? rest))
                      (rest* (drop-while kw? rest)))
                  (append (list 'deftype
                                (cons start end)
                                attrs
                                (car (transform-type rest*)))
                          (map (lambda (vrn)
                                 ;; Wrap singleton variants in a list
                                 (if (list? vrn)
                                     (map transform-type vrn)
                                     (list (transform-type vrn))))
                               (cdr rest*))))
                'invalid)]
           [($ token 'ident "defstruct" start end)
            (if (valid? (cons 'defstruct rest))
                (append (list 'defstruct
                              (cons start end)
                              (take-while kw? rest))
                        (map transform-type (drop-while kw? rest)))
                'invalid)]
           [($ token 'ident "decl" start end)
            (if (valid? (cons 'decl rest))
                (list 'decl
                      (cons start end)
                      (car rest)
                      (transform-type (cadr rest)))
                'invalid)]
           ;; Language constructs should be transformed:
           ;;   (#tok<ident cns start-end> rest ...)
           ;; =>
           ;;   (cns (start . end) rest ...)
           ;; So that they can be read directly by the Lsrc reaer
           [(and ($ token 'ident x-val start end)
                 (? (lambda (x) (member (string->symbol x-val) Lsrc-language-constructs))))
            (let ((s (string->symbol x-val)))
              (if (valid? (cons s rest))
                  (cons s (cons (cons start end)
                                (match s
                                  ['lambda (cons (take-while kw? rest)
                                                 (map transform (drop-while kw? rest)))]
                                  [:_ (map transform rest)])))
                  'invalid))]
           ;; Transform non-construct s-expressions recursively
           [else (map transform s-exp)])]
        [else s-exp]))

    (let ((srcs (map transform src)))
      (if (null? syntax-errors)
          (values (map read-Lsrc srcs) #f)
          (values #f syntax-errors))))

  ;; Converts all tokens that were allowed to be either a symbol or a qualified symbol
  ;; to be a qualified symbol.
  (define-pass coecere-symbols-to-qs
    : Lsrc (e) -> L0 ()
    (definitions
      (define (conv u)
        (if (qualified-symbol-token? u)
            u
            (ident-token->qualified-symbol-token u))))
    (Expr : Expr (e) -> Expr ()
          [(module ,l ,u ,[body*] ...)
           `(module ,l ,(conv u)
                    ,body* ...)])
    (Pattern : Pattern (pat) -> Pattern ()
             [(p-struct ,u (,x0* ,x1*) ...) `(p-struct ,(conv u) (,x0* ,x1*) ...)]
             [(p-sum ,u ,x) `(p-sum ,(conv u) ,x)]))

  ;; We want all "bodies" in our language constructs from now on to be a single expression
  ;; that means wrapping anything that isn't a single expression in a `(begin)'.
  ;;
  ;; Example:
  ;; (define (debug x) (print x) x) => (define (debug x) (begin (print x) x))
  ;; (define (id x) x) => (define (id x) x) ;; Nothing should happen here
  (define-pass make-do-explicit
    : L0 (e) -> L1 ()
    (definitions
      (with-output-language (L1 Expr)
        (define (->do body*)
          (cond
           [(null? body*) unit]
           [(null? (cdr body*)) (car body*)]
           [else `(do #f ,body* ...)]))))
    (Expr : Expr (e) -> Expr ()
          [(let ,l ([,x* ,[e*]] ...) ,[body*] ...)
           `(let ,l ([,x* ,e*] ...) ,(->do body*))]
          [(lambda ,l (,kw* ...) (,x* ...) ,[body*] ...)
           `(lambda ,l (,kw* ...) (,x* ...) ,(->do body*))]
          [(def ,l [,x ,x* ...] ,[body*] ...)
           `(def ,l (,x ,x* ...) ,(->do body*))]
          [(def ,l ,x ,[body*] ...)
           `(def ,l ,x ,(->do body*))]
          [(match ,l ,[e] (,[p*] ,[body**] ...) ...)
           (let ((body* (map ->do body**)))
             `(match ,l ,e (,p* ,body*) ...))]
          [(if ,l ,[e0] ,[e1]) `(if ,l ,e0 ,e1 ,unit)]))

  (define-pass check-attributes
    : L1 (e) -> L2 ()
    (definitions
      (define (check kw* v n)
        (make-keyword-set v kw* (lambda (kw)
                                          (errorf 'check-attributes
                                                  "Unsupported attribute for ~A: ~A" n kw)))))
    (Expr : Expr (e) -> Expr ()
          [(lambda ,l (,kw* ...) (,x* ...) ,[body*])
           `(lambda ,l ,(check kw* '("once") "lambda") (,x* ...) ,body*)])
    (TypeDef : TypeDef (tydef) -> TypeDef ()
             [(deftype ,l (,kw* ...) ,[tyname] (,x* ,[tyref**] ...) ...)
              `(deftype ,l ,(check kw* '("unique") "deftype") ,tyname (,x* ,tyref** ...) ...)]
             [(defstruct ,l (,kw* ...) ,[tyname] (,x* ,[tyref*]) ...)
              `(defstruct ,l ,(check kw* '("unique") "defstruct") ,tyname (,x* ,tyref*) ...)]))

  ;; Build our internal representation of modules (see `rmodule`) based on their syntactic form.
  ;; Modules are stored by their qualified symbol and keep track of all variables, functions, types
  ;; and toplevel expressions defined in them.
  (define-pass build-module
    : L2 (e) -> L2 (errs)
    (definitions
      (define errors '())
      (define (maybe-fail err)
        (if err (set! errors (cons err errors)))
        err)
      (define (check-duplicates module x* what)
        (let ((vrns (delete-duplicates x* equal?)))
          (if (not (equal? vrns x*))
              (make-compilation-error module
                                      (list what e
                                            (lset-difference equal? x* vrns)))
              #f)))
      (define (check-and-make-poly module tyname inner l)
        (let* ((tyvars (free-variables inner)))
          (if (not (null? (lset-difference equal?
                                           (lset-xor equal? (type-name-arguments tyname)
                                                     tyvars)
                                           (type-name-arguments tyname))))
              (make-compilation-error module
                                      (list 'mismatching-type-variables
                                            (type-name-arguments tyname)
                                            tyvars))
              (if (null? (type-name-arguments tyname))
                  inner
                  (make-poly-type (type-name-arguments tyname) inner l)))))
      (define (type-name tok args)
        (let ((name (token-value tok))
              (loc (token-span tok)))
          (or (hashtable-ref builtin-types name #f)
              (make-type-name name args loc)))))
    (Toplevel : Toplevel (z) -> Toplevel (errs)
              [,e (let ((_ (Expr e (get-toplevel-module) 'item)))
                    (values e errors))]
              [,tydef (let ((_ (TypeDef tydef (get-toplevel-module))))
                        (values tydef errors))]
              [(decl ,l ,x ,tyref) (let* ((ty (TypeRef tyref))
                                          (ty* (if (function-type? ty)
                                                   (generalize ty)
                                                   ty)))
                                     (maybe-fail (->> (declaration-from-type x ty*)
                                                      (add-module-definition (get-toplevel-module))))
                                     (values z errors))])
    (Expr : Expr (e module ctx) -> Expr ()
          [(module ,l ,q ,e* ...)
           (if (cond
                [(toplevel-qualified-symbol? module) (maybe-fail (create-new-module-with-locs q l))]
                [(eq? ctx 'def) (errorf 'build-module "Module definitions are not allowed inside defs (~A): ~A" l q)]
                [else (errorf 'build-module "Nested modules are not allowed (~A): ~A" l q)])
               e
               `(module ,l ,q ,(map (lambda (x) (Expr x (token-value q) ctx)) e*) ...))]
          ;; TODO: Make sure function/variable redefinition causes a compile error
          [(def ,l ,x ,body)
           (begin
             (when (eq? ctx 'item)
               (maybe-fail (->> (variable-definition-from-source x body)
                                (add-module-definition module))))
             (Expr body module 'def))]
          [(def ,l [,x ,x* ...] ,body)
           (begin (when (eq? ctx 'item)
                    (maybe-fail (->> (function-definition-from-source x x* body)
                                     (add-module-definition module))))
                  (Expr body module 'def))]
          [else (begin (when (eq? ctx 'item)
                         (add-module-toplevel module e))
                       e)])
    (TypeDef : TypeDef (tydef module) -> TypeDef ()
             [(deftype ,l ,attrs ,[tyname -> t0] (,x* ,[tyref** -> t**] ...) ...)
              (maybe-fail (or (check-duplicates module x* 'duplicate-variants)
                              (let* ((inner (make-sum-type
                                             (map (lambda (vrn tys)
                                                    (match tys
                                                      [() (cons vrn builtin-type-unit)]
                                                      [(t) (cons vrn t)]
                                                      [(ts ...)
                                                       (errorf 'build-module
                                                               "Tuple like sum types are currently not supported: ~A"
                                                               t0)]))
                                                  (map token-value x*)
                                                  t**)
                                             (true? (keyword-set-ref attrs "unique"))
                                             l))
                                     (ty (check-and-make-poly module t0 inner l)))
                                (if (compilation-error? ty)
                                    ty
                                    (add-module-type module
                                                     t0
                                                     ty)))))]
             [(defstruct ,l ,attrs ,[tyname -> t0] (,x* ,[tyref* -> t*]) ...)
              (maybe-fail (or (check-duplicates module x* 'duplicate-fields)
                              (let* ((inner (make-struct-type
                                             (map token-value x*)
                                             t*
                                             (true? (keyword-set-ref attrs "unique"))
                                             l))
                                     (ty (check-and-make-poly module t0 inner l)))
                                (if (compilation-error? ty)
                                    ty
                                    (add-module-type module
                                                     t0
                                                     ty)))))])
    (TypeName : TypeName (tyname) -> type ()
              [,x (type-name x '())]
              [,unit (type-name unit '())]
              [(,x ,[tyvar* -> t*] ...) (type-name x t*)])
    (TypeVar : TypeVar (tyvar) -> type ()
             [(|'| ,l ,x) (make-type-variable (token-value x) #f #;TODO: l)])
    (TypeRef : TypeRef (tyref) -> type ()
             [,x (type-name x '())]
             [,unit (type-name unit '())]
             [,tyvar (TypeVar tyvar)]
             [(,x ,[tyref* -> t*]...) (type-name x t*)]
             [(-> ,l (,[tyref0* -> t0*] ...) ,[tyref1 -> t1])
              (make-function-type t0* t1 #f #;TODO: l)])
    (Toplevel e))

  ;; ---------------------------------- Passes ----------------------------------

  (define (gather-toplevel-expressions mod)
    (let* ((mod-name (rmodule-name mod))
           (toplevels (rmodule-toplevel mod))
           (toplevel-body (with-output-language (L2 Expr)
                                                `(do #f ,(reverse toplevels) ...))))
      (add-module-definition mod-name
                             (function-definition-from-source (token-wrap ";--toplevel")
                                                              '()
                                                              toplevel-body))
      (rmodule-toplevel-set! mod '())))

  ;; We want to avoid shallowly nested dos in order to keep things simple, so if any sub-expression
  ;; of a do is another do, all it's contents get's inserted into the parent do. We also
  ;; eliminate singleton dos since they serve no purpose.
  ;;
  ;; Example:
  ;; (do 3 (do 2 1) 0) => (do 3 2 1 0)
  ;; (do (do 0)) => 0
  (define-pass flatten-dos
    : L2 (e mod def) -> L2 ()
    (Expr : Expr (e) -> Expr ()
          (definitions
            (define flatten
              (lambda (e e*)
                (nanopass-case (L2 Expr) e
                  [(do ,l ,body* ...)
                   (flatten (car body*) (append (cdr body*) e*))]

                  [else (if (null? e*)
                            (list e)
                            (cons e (flatten (car e*) (cdr e*))))])))
            (define is-unit?
              (lambda (x)
                (and (token-kind? x 'ident)
                     (string=? (token-value x)
                               "unit"))))
            (define remove-non-final-units
              (lambda (e*)
                (if (null? e*)
                    '()
                    (let next ((rest e*))
                      (if (null? rest)
                          '()
                          (if (is-unit? (car rest))
                              (if (null? (cdr rest))
                                  rest
                                  (next (cdr rest)))
                              (cons (car rest)
                                    (next (cdr rest))))))))))

          [(do ,l ,[body*] ...)
           (if (null? body*)
               unit
               (let ((body1* (remove-non-final-units (flatten (car body*) (cdr body*)))))
                 (cond
                  [(null? body1*) unit]
                  [(null? (cdr body1*)) (car body1*)]
                  [else `(do ,l ,body1* ...)])))]))

  (define (make-generic-type-name mod ty-name)
    (make-type-name
     ;; Use a type-name instead of a qualified symbol
     (token-value ty-name)
     (let ((ty-def (get-module-type (rmodule-name mod) (token-value ty-name))))
       (if (poly-type? ty-def)
           ;; If the referenced type is a poly-type, then
           ;; create an instanciation with fresh type-variables
           (map (lambda (x)
                  (make-type-variable
                   (unique-name (type-variable-name x))
                   (linear? x)
                   #f))
                (poly-type-type-variables ty-def))
           '()))
     (token-span ty-name)))

  ;; If it's a type-name, then resolve it to it's definition
  ;; WARNING: This should only be done to check the underlying structure of a type
  ;; Type names are still nominal and (deftype MyIntPair (, int int)) =/= (, int int)
  (define (resolve mod ty)
    (if (type-name? ty)
        (type-application (get-module-type (rmodule-name mod)
                                           (type-name-name ty))
                          (type-name-arguments ty))
        ty))

  (define-pass lower-type-constructors
    : L2 (e mod def) -> L3 ()
    (definitions
      (define (type-defined? t)
        (hashtable-contains? (rmodule-types mod) t))
      (define (is-inner? p? ty)
        (or (p? ty)
            (and (poly-type? ty)
                 (p? (poly-type-inner-type ty)))))
      (with-output-language (L3 Expr)
        (define (create-construct tyn* flds vals)
          (let ((tyn (if (ident? tyn*) (token-value tyn*)
                         (qualified-symbol-identifier (token-value tyn*)))))
            (if (type-defined? tyn)
                (let ((ty (get-module-type (rmodule-name mod) tyn)))
                  (if (is-inner? struct-type? ty)
                      `(construct ,(token-span tyn*)
                                  ,(make-generic-type-name mod
                                                           (make-token 'ident
                                                                       tyn
                                                                       (token-start tyn*)
                                                                       (token-end tyn*)))
                                  (,flds ,vals) ...)
                      (module-fail mod (list 'invalid-constructor e))))
                ;; We'll let the variable renaming pass take care of failing for us
                `(,tyn* (,flds ,vals) ...))))
        (define (create-variant q e)
          (let ((tyn (car (qualified-symbol-namespace (token-value q))))
                   (vrn (qualified-symbol-identifier (token-value q))))
               (if (type-defined? tyn)
                   (let ((ty (get-module-type (rmodule-name mod) tyn)))
                     (if (is-inner? sum-type? ty)
                         `(variant ,(token-span q)
                                   ,(make-generic-type-name mod
                                                            (make-token 'ident
                                                                        tyn
                                                                        (token-start q)
                                                                        (token-end q)))
                                   ,(make-token 'ident vrn
                                                (token-start q)
                                                (token-end q))
                                   ,e)
                         (module-fail mod (list 'invalid-constructor q))))
                   `(,(Expr q) ,e))))))
    (Expr : Expr (e^) -> Expr ()
          [,q (module-fail mod (list 'unsupported-qualification q))]
          [(,e (,x* ,[e*]) ...)
           (cond
            [(or (ident? e)
                 (and (token? e)
                      (qualified-symbol? (token-value e))
                      (null? (qualified-symbol-namespace (token-value e)))))
             (create-construct e x* e*)]
            [(and (token? e)
                  (qualified-symbol? (token-value e))
                  (= (length (qualified-symbol-namespace (token-value e))) 1)
                  (<= (length x*) 1))
             (create-variant e (if (null? x*)
                                   unit
                                   `(,(Expr (car x*)) ,(car e*))))]
            [else `(,(Expr e) (,x* ,e*) ...)])]
          [(,e0 ,[e1])
           (if (and (token? e0)
                    (qualified-symbol? (token-value e0))
                    (= (length (qualified-symbol-namespace (token-value e0))) 1))
               (create-variant e0 e1)
               `(,(Expr e0) ,e1))]))

  ;; Scoping function used for hanlding basic alist based scopes
  (define (push-scope scope elms)
    (cons elms scope))

  (define (pop-scope scope)
    (cdr scope))

  (define (lookup scope x . mwrapper)
    (define wrapper (if (null? mwrapper) (lambda (_ x) x) (car mwrapper)))
    (define (finder scope x)
      (if (null? scope)
          #f
          (or (assoc x (car scope))
              (finder (cdr scope) x))))
    (let ((result (finder scope (token-value x))))
      (if result
          ;; TODO: This doesn't work outside make-unqieue-whate-ver
          (wrapper x result)
          #f)))

  (define (print-scope scope)
    (for-each (lambda (level)
                (printf "----\n")
                (for-each (lambda (entry)
                            (printf "  ~A:\t\t~A\n" (car entry) (cdr entry)))
                          level))
              scope)
    (printf "----/\n"))

  (define (lookup* scope x . mwrapper)
    (define wrapper (if (null? mwrapper) (lambda (_ x) x) (car mwrapper)))
    #;(print-scope scope)
    (let ((result (lookup scope x wrapper)))
      (if result
          result
          (errorf 'lookup*
                  "Internal compiler error: Expected identifier `~A` to be bound" x))))

  ;; For `do` expressions, a line might introduce a definition, and the newly
  ;; created scope is to be passed along to the following expressions
  (define (pass-along-scope scope elms scope-of . context)
    (let next ((scope* scope)
               (elms elms)
               (context context)
               (body '()))
      (if (null? elms)
          (apply values (cons (reverse body) context))
          (let*-values ([(elm) (car elms)]
                        [_ (apply list scope* elm context)]
                        [(scope** new-expr . context*) (apply scope-of (cons scope* (cons elm context)))])
            ;; Run the pass recursively on all the sub-expressions of the begin
            ;; but with the accumilated scope passed along
            (next scope** (cdr elms) context* (cons new-expr body))))))

  ;; Used for evaluating expressions in let-bindings where only previous
  ;; bindings (from the same let) are visible
  ;; xs: binding names
  ;; es: binding values
  ;; scope: initial scope
  ;; entry: (x new-x new-e) -> scope-entry
  ;; xf: binding name transformer (Can be #f)
  ;; ef: binding value transformer (usually just Expr)
  (define (let-scope-fold xs es scope entry xf ef)
    (let next-binding
        ([xs xs]
         [es es]
         [scope (push-scope scope '())])
      (if (null? xs)
          (values '() '() scope)
          (let* ((x (car xs))
                 (e (car es))
                 (new-x (if xf (xf x) x))
                 (new-e (ef e scope))
                 ;; Add the current binding to the scope
                 (new-scope (push-scope (pop-scope scope)
                                        (cons (entry x new-x new-e)
                                              (car scope)))))
            (let-values ([(xs* es* scope*) (next-binding (cdr xs)
                                                         (cdr es)
                                                         new-scope)])
              ;; Rebuild x* and e* with renamings
              (values (cons new-x xs*)
                      ;; NOTE: we don't give e new-scope because
                      ;; that would allow for a recursive binding
                      (cons new-e es*)
                      ;; We bubble up the last scope (which contains all bindings)
                      scope*))))))


  ;; We want to make sure that all variable names are unique, since that'll make name
  ;; lookups much simpler in future passes. As a side effect we'll also find undefined
  ;; variable references in this pass. Top-level names are not renamed. Renaming names
  ;; are postfixed with a ".<number>" sequence, which allows for a simple extraction
  ;; of the original source name.
  ;;
  ;; (define (do-things x) (let ((x 3)) (print x)) x) =>
  ;;   (define (do-things x.0) (let ((x.1 3)) (print x.1)) x.0)
  (define-pass make-variables-unique
    : L3 (e mod def) -> L3 ()
    (definitions
      (define (toplevel-scope)
        ;; TODO: Exclude toplevel variables from referencing themselves
        (define module-scope (map (lambda (x)
                                    (cons x x))
                                  (vector->list (hashtable-keys (rmodule-definitions mod)))))
        (define function-scope (if (function-definition? def)
                                   (map (lambda (x)
                                          (entry x (make-argument x)))
                                        (function-definition-arguments def))
                                   '()))
        (if (null? function-scope)
            (list module-scope)
            (list function-scope module-scope)))
      (define (make-argument x)
        (token-map (lambda (x) (string-append x ".arg")) x))
      (define (make-unique x)
        (token-map unique-name x))
      (define (entry x y)
        (cons (token-value x) (token-value y)))
      (define (introduce-new x)
        (list (entry x (make-unique x))))
      (define (wrap-name x x*)
        (token-inherit (cdr x*) x))
      ;; Get the scopes (inner and outer) produced from the expression (if any)
      (define (scope-of scope x)
        (define (extract-name e)
          (nanopass-case (L3 Expr) e
            [(def ,l ,x ,body) x]
            [(def ,l [,x ,x* ...] ,body) x]
            [else #f]))
        (let ((old-name (extract-name x))
              (x* (Expr x scope)))
          ;; If the expression was one defining a name, record the old one
          ;; then make it unique (recursively) and create a new scope with the new name
          (if old-name
              (values (push-scope scope (list (entry old-name
                                                     (extract-name x*))))
                      x*)
              (values scope x*)))))
    (Expr : Expr (e scope) -> Expr ()
          ;; Look up identifiers
          [,x (let ((x* (lookup scope x wrap-name)))
                (or x* (module-fail mod (list 'undefined-variable x))))]
          ;; Build up a scope sequencially in a `begin`
          [(do ,l ,body* ...)
           `(do ,l ,(pass-along-scope scope body* scope-of) ...)]
          ;; Rename both function name and parameters
          [(def ,l [,x ,x* ...] ,body)
           (let ((duplicate-argument (find-duplicate x* token-value)))
             (if duplicate-argument
                 (module-fail mod (list 'duplicate-argument duplicate-argument))
                 (let* ((new-name (make-unique x))
                        (new-params (map* (lambda (x) (make-unique x)) x*)))
                   `(def ,l [,new-name ,new-params ...]
                         ,(Expr body (push-scope scope
                                                 (cons (entry x new-name)
                                                       (map entry x* new-params))))))))]
          ;; Rename variable
          [(def ,l ,x ,[body])
           `(def ,l ,(make-unique x) ,body)]
          ;; Rename all bindings
          [(let ,l ((,x* ,e*) ...) ,body)
           (let ((duplicate-binding (find-duplicate x* token-value)))
             (if duplicate-binding
                 (module-fail mod (list 'already-defined duplicate-binding))
                 ;; Since each binding should be able to see the binding before
                 ;; it, we need to start with an empty scope, and fold it over
                 ;; each binding, and then rename the body with the final binding
                 (let-values ([(x** e** scope*)
                               (let-scope-fold x* e* scope
                                               (lambda (x new-x _)
                                                 (entry x new-x))
                                               make-unique
                                               Expr)])
                   `(let ,l ((,x** ,e**) ...)
                         ,(Expr body scope*)))))]
          ;; Rename the arguments
          [(lambda ,l ,attrs (,x* ...) ,body)
           (let ((duplicate-argument (find-duplicate x* token-value)))
             (if duplicate-argument
                 (module-fail mod (list 'duplicate-argument duplicate-argument))
                 (let* ((new-params (map* make-unique x*)))
                   `(lambda ,l ,attrs (,new-params ...)
                            ,(Expr body (push-scope scope (map entry x* new-params)))))))]
          ;; Rename all bindings
          [(match ,l ,[e] (,p* ,body*) ...)
           (let-values ([(p** bindings) (map-values2 Pattern p*)])
             (let ((body** (map (lambda (body bindings)
                                  (Expr body (push-scope scope bindings)))
                                body*
                                bindings)))
               `(match ,l ,e (,p** ,body**) ...)))])
    (Pattern : Pattern (p) -> Pattern (bindings)
             [(p-struct ,q (,x0* ,x1*) ...)
              (let ((duplicate-binding (find-duplicate x0* token-value)))
                (if duplicate-binding
                    (module-fail mod (list 'already-defined duplicate-binding))
                    ;; Make sure we have all the struct's fields, and only them
                    (let ((new-bindings (map* make-unique x0*)))
                      (values `(p-struct ,q (,new-bindings ,x1*) ...)
                              (map entry x0* new-bindings)))))]
             [(p-sum ,q ,x)
              (let ((x* (and x (make-unique x))))
                (values `(p-sum ,q ,x*)
                        (if x* (list (entry x x*)) '())))])

    (let ((e* (Expr e (toplevel-scope))))
      ;; Rename the argument names in the definition if it's a function
      (when (function-definition? def)
        (function-definition-arguments-set!
         def
         (map make-argument (function-definition-arguments def))))
      e*))

  (define-pass lower-match
    : L3 (e mod def) -> L4 ()
    (definitions
      (define (resolve-inner n)
        (let ((ty (get-module-type (rmodule-name mod) n)))
          (if (poly-type? ty)
              (poly-type-inner-type ty)
              ty))))
    (Expr : Expr (e) -> Expr ()
          [(match ,l ,[e0] (,p* ,[body*]) ...)
           ;; This shouldn't be syntacticly possible, but another
           ;; pass could possibly generate this form, so we should
           ;; handle it for completeness sake
           (if (null? body*)
               ;; We this keep the matched expression (so it can be linearity checked)
               ;; but we throw it out
               `(do ,l ,e0 unit)
               (let check-next-arm ((ps p*)
                                    (ty-name #f)
                                    (ty #f)
                                    (struct-pat #f)
                                    (sum-cases '()))
                 (match ps
                   ['() (let* ((ty* (make-generic-type-name mod ty-name)))
                          (if struct-pat
                              (let-values ([(vars flds) (unzip2 struct-pat)])
                                `(destruct ,l ,e0 (,ty* (,vars ,flds) ...) ,(car body*)))
                              (let-values ([(vrnt vars) (unzip2 (reverse sum-cases))])
                                `(case ,l ,e0 ,ty* ((,vrnt ,vars) ,body*) ...))))]
                   [(p . ps)
                    (nanopass-case (L3 Pattern) p
                      [(p-struct ,q (,x0* ,x1*) ...)
                       (cond
                        ;; Since match right now is not really a pattern matching construct
                        ;; but a destructuring construct, any match on a struct must be a the only arm
                        [struct-pat (module-fail mod (list 'multiple-struct-match-arms struct-pat p))]
                        [(not (null? sum-cases)) (module-fail mod (list 'mismatching-match-arms sum-cases p))]
                        [else (let ((q* (token-value q)))
                                (if (not (null? (qualified-symbol-namespace q*)))
                                    (module-fail mod (list 'unsupported-qualification q*))
                                    (let ((struct-ty (resolve-inner (qualified-symbol-identifier q*))))
                                      (if (struct-type? struct-ty)
                                          ;; Check that the rest of the arms are valid
                                          (check-next-arm ps (token-inherit* (qualified-symbol-identifier q*)
                                                                             q 'ident)
                                                          struct-ty (zip x0* x1*) '())
                                          (module-fail mod (list 'invalid-match-pattern p q))))))])]
                      [(p-sum ,q ,x)
                       (if struct-pat
                           (module-fail mod (list 'mismatching-match-arms (list struct-pat) p))
                           (let ((q* (token-value q)))
                             (match (qualified-symbol->list q*)
                               [(:_) (module-fail mod (list 'invalid-match-pattern p q))]
                               [(ty-n vrt)
                                (let ((sum-ty (resolve-inner ty-n)))
                                  (if (sum-type? sum-ty)
                                      (if (or (not ty) (equal? sum-ty ty))
                                          (check-next-arm ps (or ty-name
                                                                 (token-inherit* ty-n q 'ident))
                                                          sum-ty #f
                                                          (cons (list
                                                                 (token-inherit* vrt q 'ident)
                                                                 (or x (token-inherit* (unique-name "_")
                                                                                       q 'ident)))
                                                                sum-cases))
                                          (module-fail mod (list 'mismatching-match-arms sum-cases p)))
                                      (module-fail mod (list 'invalid-match-pattern p q))))]
                               [else (module-fail mod (list 'unsupported-qualification q*))])))])])))]))

  (define (extract-type-and-value T)
    (nanopass-case (L5 Typed) T
      [(: ,e ,t) (values e t)]
      ;; If we encounter an invalid AST node, just propegate it
      [,i (values i i)]
      [else (errorf 'extract-type-and-value
                    "Got unexpected Typed value: ~A"
                    T)]))

  (define (expr-of T)
    (let-values ([(e _) (extract-type-and-value T)])
      e))

  (define (type-of T)
    (let-values ([(_ t) (extract-type-and-value T)])
      t))

  (define (analyze-call-graph mod)
    (define-pass gather-dependencies
      : L4 (e def) -> * ()
      (definitions
        (define module-definitions
          (hash-table-keys defs)))
      (Expr : Expr (e) -> * ()
            [,k '()]
            [,b '()]
            [,q '()]
            [,x (let ((v (token-value x)))
                  (if (list-index (lambda (y) (string=? (token-value x) y))
                                  module-definitions)
                      (list (cons def v))
                      '()))]
            [(module ,l ,q ,body* ...) (concatenate (map Expr body*))]
            [(do ,l ,body* ...) (concatenate (map Expr body*))]
            [(if ,l ,e0 ,e1 ,e2)
             (append (Expr e0)
                     (Expr e1)
                     (Expr e2))]
            [(lambda ,l ,attrs (,x* ...) ,body) (Expr body)]
            [(let ,l ([,x ,e*] ...) ,body)
             (concatenate (cons (Expr body)
                                (map Expr e*)))]
            [(def ,l [,x ,x* ...] ,body) (Expr body)]
            [(def ,l ,x ,body) (Expr body)]
            [(destruct ,l ,e (,t (,x0* ,x1*) ...) ,body)
             (append (Expr e)
                     (Expr body))]
            [(construct ,l ,t (,x* ,e*) ...)
             (concatenate (map Expr e*))]
            [(variant ,l ,t ,x ,e)
             (Expr e)]
            [(case ,l ,e0 ,t ((,x0 ,x1) ,e*) ...)
             (concatenate (cons (Expr e0)
                                (map Expr
                                     e*)))]
            [(,e ,e* ...)
             (concatenate (cons (Expr e)
                                (map Expr e*)))]
            [,i
             '()]))

    (define defs (rmodule-definitions mod))

    (define depedency-graph
      (hash-table-fold defs
                       (lambda (def-name def edges)
                         (if (definition-body def)
                             (append (gather-dependencies (definition-body def)
                                                          def-name)
                                     edges)
                             edges))
                       '()))
    (define rec-groups
      (recursive-dependency-groups (hash-table-keys defs) depedency-graph))

    ;; Right now we just bundle the actual graph with the dependency groupings
    ;; We might want something more structured later
    (rmodule-call-graph-set! mod (cons depedency-graph
                                       rec-groups)))

  (define-pass substitute-types
    : L5 (e subs) -> L5 ()
    (Expr : Expr (e) -> Expr ()
          ;; Each language construct that stores a type (for bindings)
          ;; Needs that type updated in too:
          [(def ,l (,x (,x* ,t*) ...) ,t ,[E])
           `(def ,l (,x (,x* ,(map (lambda (t) (apply-substitutions subs t)) t*)) ...)
                 ,(apply-substitutions subs t)
                 ,E)]
          [(def ,l ,x ,t ,[E])
           `(def ,l ,x ,(apply-substitutions subs t) ,E)]
          [(let ,l ([,x* ,t* ,[E*]] ...) ,[E])
           `(let ,l ([,x* ,(map (lambda (t) (apply-substitutions subs t)) t*) ,E*] ...) ,E)]
          [(lambda ,l ,attrs ((,x* ,t*) ...) ,[E])
           `(lambda ,l ,attrs ((,x* ,(map (lambda (t) (apply-substitutions subs t)) t*)) ...) ,E)]
          [(destruct ,l ,[E0] (,t (,x0* ,x1*) ...) ,[E1])
           `(destruct ,l ,E0 (,(apply-substitutions subs t) (,x0* ,x1*) ...) ,E1)]
          [(construct ,l ,t (,x* ,[E*]) ...)
           `(construct ,l ,(apply-substitutions subs t) (,x* ,E*) ...)]
          [(variant ,l ,t ,x ,[E])
           `(variant ,l ,(apply-substitutions subs t) ,x ,E)]
          [(case ,l ,[E0] ,t ((,x0 ,x1) ,[E*]) ...)
           `(case ,l ,E0 ,(apply-substitutions subs t) ((,x0 ,x1) ,E*) ...)])
    ;; Each typed expression needs its annotated type updated
    (Typed : Typed (T) -> Typed ()
           [(: ,[e] ,t) `(: ,e ,(apply-substitutions subs t))]))

  (define-pass free-variables-in-expr
    : L5 (e) -> * ()
    (definitions
      (define (merge a bs)
        (fold-left (lambda (acc x)
                     (lset-union equal? acc x))
                   a
                   bs)))
    (Expr : Expr (e) -> * ()
          [,k '()]
          [,x '()]
          [,b '()]
          [(do ,l ,E* ...) (merge '() (map Typed E*))]
          [(if ,l ,E0 ,E1 ,E2) (lset-union equal?
                                           (Typed E0)
                                           (Typed E1)
                                           (Typed E2))]
          [(def ,l ,x ,t ,E) (lset-union equal?
                                         (free-variables t)
                                         (Typed E))]
          [(def ,l (,x (,x* ,t*) ...) ,t ,E)
           (merge (Typed E)
                  (map free-variables (cons t t*)))]
          [(let ,l ([,x* ,t* ,E*] ...) ,E)
           (-> (Typed E)
               (merge (map Typed E*))
               (merge (map free-variables t*)))]
          [(lambda ,l ,attrs ((,x* ,t*) ...) ,E)
           (-> (Typed E)
               (merge (map free-variables t*)))]
          [(destruct ,l ,E0 (,t (,x0* ,x1*) ...) ,E1)
           (lset-union equal?
                       (free-variables t)
                       (free-variables E0)
                       (free-variables E1))]
          [(construct ,l ,t (,x* ,E*) ...)
           (-> (free-variables t)
               (merge (map Typed E*)))]
          [(variant ,l ,t ,x ,E)
           (lset-union equal?
                       (free-variables t)
                       (Typed E))]
          [(case ,l ,E0 ,t [(,x0 ,x1) ,E*] ...)
           (-> (lset-union equal?
                           (Typed E0)
                           (free-variables t))
               (merge (map Typed E*)))]
          [(,E ,E* ...)
           (-> (Typed E)
               (merge (map Typed E*)))]
          [,i (errorf 'free-variables-in-expr "Encountered invalid AST node")]
          [else '()])
    (Typed : Typed (T) -> * ()
           [(: ,e ,t) (lset-union equal? (Expr e) (free-variables t))]
           [,i (errorf 'free-variables-in-expr "Encountered invalid AST node")]))

  (define (annotate-types mod)
    (define (chain-contexts f ctx e*)
      (let next ([e* e*]
                 [ctx ctx]
                 [T* '()])
        (if (null? e*)
            (values (reverse T*) ctx)
            (let-values ([(T ctx*) (f (car e*) ctx)])
              (next (cdr e*) ctx* (cons T T*))))))

    (define (type-of-constant k)
      (case (token-kind k)
        [(number) (if (exact? (token-value k))
                      builtin-type-int
                      builtin-type-float)]
        [(string) builtin-type-string]
        [(keyword) (errorf 'annotate-types
                                  "Unsupported ~A constant: ~A"
                                  (token-kind k)
                                  k)]
        [else (errorf 'annotate-types "Unexpected token constant: ~A" k)]))

    (define (type-of-builtin b)
      (builtin-info-type (hash-table-ref builtins (token-value b))))

    (define (solve-constraints constraints)
      (let* ((unsatisfiables '())
             (subs* (fold-left
                     (lambda (subs constraint)
                       (or (let ((uni-subs (unify-types (car constraint)
                                                        (cdr constraint))))
                             (and uni-subs (unite-substitutions subs uni-subs)))
                           (begin (push-set! unsatisfiables
                                             (cons (apply-substitutions subs (car constraint))
                                                   (apply-substitutions subs (cdr constraint))))
                                  subs)))
                     '()
                     constraints)))
        (if (null? unsatisfiables)
            subs*
            (begin (for-each (lambda (unsat)
                               (let ((a (car unsat))
                                     (b (cdr unsat)))
                                 (module-fail mod (list (if (occours-in? a b)
                                                            'recursive-type
                                                            'unsatisfiable-constraint)
                                                        a b))))
                             unsatisfiables)
                   #f))))

    (define-pass infer-types
      : L4 (e def scope self) -> L5 (ty unis subs)
      (definitions
        (define unifications '())
        (define (uni a b)
          (set! unifications (cons (cons a b)
                                   unifications)))
        (define (scope-of scope x)
          (let* ([E (Expr x scope)]
                 [entry? (nanopass-case (L5 Expr) (expr-of E)
                           [(def ,l (,x [,x* ,t*] ...) ,t ,E) (cons (token-value x) t)]
                           [(def ,l ,x ,t ,E) (cons (token-value x) t)]
                           [else #f])])
            ;; If there's an entry, add it to a scope
            (values (if entry?
                        (push-scope scope (list entry?))
                        scope)
                    E)))
        (define (infer e scope)
          (let-values ([(E ty _ subs) (infer-types e #f scope #f)])
            ;; Since substitutions are fully resolved (read simplified) unifcations
            ;; of that given context, then they're also valid unifcations in themselves
            ;; So it should be valid (and more efficient) to simply add the substitutions
            ;; (instead of the unifcations) to the current context's unifcations
            (set! unifications (append subs unifications))
            E))
        (define (bound-type-variables scope)
          (fold-left (lambda (acc s)
                       (fold-left (lambda (acc binding)
                                    (lset-union equal? acc (free-variables (cdr binding))))
                                  acc
                                  s))
                     '()
                     scope)))
      (Expr : Expr (e scope) -> Typed ()
            ;; Type constants
            [,k `(: ,k ,(type-of-constant k))]
            [,b `(: ,b ,(type-of-builtin b))]
            ;; Lookup the type based on the current typing scope
            ;; If a variable is not found, then that's a bug and an ICE
            [,x (let ((ty (instanciate (cdr (lookup* scope
                                                     x
                                                     (lambda (_ x) x))))))
                  (if (invalid-ast? ty)
                      ty
                      `(: ,x ,ty)))]
            ;; Not supported:
            [(module ,l ,q ,body* ...) #;TODO:(make-invalid-ast)]
            ;; Introduce the arguments into the type-scope for the body
            ;; The def itself, isn't really an expression, but we type
            ;; it as unit for now, we should probably hoist them (TODO)
            [(def ,l [,x ,x* ...] ,body)
             ;; TODO: We don't unify properly since we're generalizing
             (let* ([t* (map (lambda (_) (new-var)) x*)]
                    [rty (new-var)]
                    [fty (make-function-type t*
                                             rty
                                             #f
                                             #f)]
                    [body-scope (push-scope scope
                                            (cons (cons (token-value x) fty)
                                                  (map (lambda (param ty)
                                                         (cons (token-value param)
                                                               ty))
                                                       x*
                                                       t*)))])
               (let-values ([(E ty unis subs) (infer-types body #f body-scope #f)])
                 (let ((subs* (cons (cons rty ty) subs)))
                   ;; Add the subs. to our unifcations, plus that the return type
                   ;; of the def is unified with type of the def body
                   (set! unifications (append subs* unifications))
                   (let* ((fty* (apply-substitutions subs* fty))
                          (def-ty (generalize fty* (bound-type-variables scope))))
                     (if (invalid-ast? def-ty)
                         def-ty
                         `(: (def ,l [,x (,x* ,t*) ...] ,def-ty
                                  ,E)
                             ,builtin-type-unit))))))]
            ;; See above
            [(def ,l ,x ,body)
             (let* ((E (infer body scope))
                    (def-ty (generalize (type-of E)
                                        (bound-type-variables scope))))
               (if (invalid-ast? def-ty)
                   def-ty
                   `(: (def ,l ,x ,def-ty
                            ,E)
                       ,builtin-type-unit)))]
            ;; Infer the type of each bound value, and generalize over it
            ;; Then infer the type of the body with the scope of the bindings
            [(let ,l ((,x* ,e*) ...) ,body)
             (let-values ([(_ E* scope*)
                           (let-scope-fold x* e* scope
                                           (lambda (x _ E)
                                             (cons (token-value x) (generalize (type-of E)
                                                                               (bound-type-variables scope))))
                                           #f
                                           infer)])
               (let ([E (Expr body scope*)]
                     ;; Just gather the types from the scope since they're already generalized
                     [t* (reverse (map cdr (car scope*)))])
                 (if (any invalid-ast? t*)
                     (make-invalid-ast)
                     `(: (let ,l ((,x* ,t* ,E*) ...)
                              ,E)
                         ,(type-of E)))))]
            [(lambda ,l ,attrs (,x* ...) ,body)
             (let* ([t* (map (lambda (_) (new-var)) x*)]
                    [body-scope (push-scope
                                 scope
                                 (map (lambda (param ty)
                                        (cons (token-value param)
                                              ty))
                                      x*
                                      t*))]
                    [E (Expr body body-scope)]
                    ;; TODO: Right now there's no way of making linear lambdas
                    [t (make-function-type t* (type-of E) (true? (keyword-set-ref attrs "once")) #f)])
               (if (or (invalid-ast? t)
                       (any invalid-ast? t*))
                   (make-invalid-ast)
                   `(: (lambda ,l ,attrs ((,x* ,t*) ...)
                           ,E)
                   ,t)))]
            ;; Check that the initializer expressions match the field types
            ;; Types as the constructed struct
            [(construct ,l ,t (,x* ,e*) ...)
             ;; Resolve the constructed type
             (let* ((struct-ty (resolve mod t))
                    ;; Type all the initializers
                    (E* (map (lambda (e) (Expr e scope)) e*)))
               (let-values ([(wrong missing)
                             (compare-sets string=?
                                           (map token-value x*)
                                           (struct-type-field-names struct-ty))])
                 ;; Validate that all fields are correct
                 (if (and (null? wrong) (null? missing))
                     (begin
                       ;; Unify all initializer values with the corresponding field type
                       (for-each (lambda (f E)
                                   (uni (type-of-field struct-ty (token-value f))
                                        (type-of E)))
                                 x* E*)
                       (if (invalid-ast? t)
                           t
                           `(: (construct ,l ,t (,x* ,E*) ...) ,t)))
                     (module-fail mod (list 'invalid-struct-fields
                                            e wrong missing)))))]
            ;; Check that the type of e is a struct type, make sure we match all fields
            ;; bind the field types for the body and type the expression a the body
            [(destruct ,l ,e0 (,t (,x0* ,x1*) ...) ,body)
             (let* ((E0 (Expr e0 scope))
                    (struct-ty (resolve mod t)))
               ;; Unify the passed value with the struct type
               (uni t (type-of E0))
               (let-values ([(wrong missing)
                             (compare-sets string=?
                                           (map token-value x1*)
                                           (struct-type-field-names struct-ty))])
                 (if (and (null? wrong) (null? missing))
                     ;; Create alist map of name -> type
                     (let* ([body-scope (push-scope
                                         scope
                                         ;; Lookup the type of a field
                                         ;; and then bind it to the var
                                         (map (lambda (binding field)
                                                (cons (token-value binding)
                                                      (type-of-field struct-ty (token-value field))))
                                              x0*
                                              x1*))]
                            [E (Expr body body-scope)])
                       (if (or (invalid-ast? (type-of E))
                               (invalid-ast? t))
                           (make-invalid-ast)
                           `(: (destruct ,l ,E0 (,t (,x0* ,x1*) ...) ,E)
                               ,(type-of E))))
                     (module-fail mod (list 'invalid-struct-fields
                                            e wrong missing)))))]
            ;; Check that we're instanciating a valid variant, and that the type matches
            ;; Type the expression as the sum type of the variant.
            [(variant ,l ,t ,x ,e0)
             (let* ((E0 (Expr e0 scope))
                    (sum-ty (resolve mod t))
                    (variant (assoc (token-value x) (sum-type-variants sum-ty))))
               (if variant
                   (begin (uni (cdr variant) (type-of E0))
                          (if (invalid-ast? t)
                              t
                              `(: (variant ,l ,t ,x ,E0)
                                  ,t)))
                   (module-fail mod (list 'incorrect-variant
                                          e (token-value x)))))]
            ;; Check that we cover all variants, unify the expression with the type and
            ;; make sure all arms have the same type.
            [(case ,l ,e0 ,t ((,x0 ,x1) ,e*) ...)
             (let* ((E0 (Expr e0 scope))
                    (sum-ty (resolve mod t))
                    (variants (sum-type-variants sum-ty)))
               (uni t (type-of E0))
               (let-values ([(wrong missing)
                             (compare-sets string=?
                                           (map token-value x0)
                                           (map car variants))])
                 (if (and (null? wrong) (null? missing))
                     (let* ((E* (map (lambda (e x0 x1)
                                       (let* ((x0* (token-value x0))
                                              (x1* (token-value x1))
                                              (var-x (cdr (assoc x0* variants))))
                                         (Expr e
                                               (push-scope scope
                                                           (list (cons x1* var-x))))))
                                     e* x0 x1)))
                       ;; Unify all arm types with the first one
                       (let ((first-arm-ty (type-of (car E*))))
                         (for-each (lambda (arm)
                                     (uni first-arm-ty (type-of arm)))
                                   (cdr E*))
                         ;; Type it as the first arm
                         (if (invalid-ast? t)
                             t
                             `(: (case ,l ,E0 ,t ((,x0 ,x1) ,E*) ...)
                             ,first-arm-ty))))
                     (module-fail mod (list 'invalid-variants
                                            e wrong missing)))))]
            ;; Gather all defs and create a scope of their types
            ;; We don't care about order here (was handeld by scoping)
            ;; Type the expression as the last value in the body
            [(do ,l ,body* ...)
             (let* ([E* (pass-along-scope scope body* scope-of)]
                    [t (type-of (last E*))])
               (if (invalid-ast? t)
                   t
                   `(: (do ,l ,E* ...)
                       ,t)))]
            ;; Simply try to unify the condition with a bool and
            ;; the branches with eachother
            [(if ,l ,e0 ,e1 ,e2)
             (let* ([T0 (Expr e0 scope)]
                    [T1 (Expr e1 scope)]
                    [T2 (Expr e2 scope)])
               (uni (type-of T0) builtin-type-bool)
               (uni (type-of T1) (type-of T2))
               (if (invalid-ast? (type-of T2))
                   (make-invalid-ast)
                   `(: (if ,l ,T0 ,T1 ,T2) ,(type-of T2))))]
            ;; Infer all expressions and try to unify the called
            ;; as a function that takes the types of the arguments
            [(,e ,e* ...)
             (let* ([T (Expr e scope)]
                    [Tty (type-of T)]
                    [T* (map* (lambda (x) (Expr x scope)) e*)]
                    [rTy (new-var)]
                    ;; TODO: Right we alway try to union was a non-linear function
                    ;; Maybe we need a '<undecided> value for linearity which
                    ;; always get's unified with a concrete value?
                    ;; Because while non-linear is the default, it also makes
                    ;; it impossible to merge with linear functions then
                    [fTy (make-function-type (map type-of T*) rTy #f #f)])
               (uni Tty fTy)
               `(: (,T ,T* ...) ,rTy))]
            [,i i])
      (let* ([T (Expr e scope)]
             [fty (and (function-definition? def)
                       (make-function-type (map cdr (car scope))
                                           (type-of T)
                                           #f #f))]
             [_ (when self
                  (uni self fty))]
             [subs (solve-constraints unifications)]
             [T* (and subs (substitute-types T subs))])
        (values (or T* (make-invalid-ast))
                (and subs (cond
                           [(function-definition? def)
                            (generalize
                             (apply-substitutions subs fty)
                             (bound-type-variables scope))]
                           [(variable-definition? def) (generalize (type-of T*)
                                                                   (bound-type-variables scope))]
                           [else (type-of T*)]))
                (or unifications '())
                subs)))

    (define defs (rmodule-definitions mod))
    (define rec-groups (cdr (rmodule-call-graph mod)))

    ;; Infer types
    (let ((toplevel-scope '(())))
      (define (annotate-definition def-name scope self)
        (let* ((def (hash-table-ref defs def-name))
               (scope (if (function-definition? def)
                          ;; Create type variable for each function argument
                          (cons (map (lambda (arg) (cons (token-value arg) (new-var)))
                                     (function-definition-arguments def))
                                scope)
                          scope)))
          (if (definition-body def)
              ;; Only annotate the type if there's a body
              (begin (enter-module-context mod def-name)
                     (let-values ([(T ty _ subs) (infer-types (definition-body def)
                                                              def
                                                              scope
                                                              self)])
                       (definition-body-set! def T)
                       (definition-type-set! def ty)
                       (set! toplevel-scope (list (cons (cons def-name ty) (car toplevel-scope))))
                       (assert (L5? (definition-body def)))
                       subs))
              ;; Otherwise it has to be a declaration with a type
              (begin (assert (definition-type def))
                     (set! toplevel-scope (list (cons (cons def-name (definition-type def))
                                                      (car toplevel-scope))))
                     '()))))
      (for-each
       (lambda (def-names)
         (if (list? def-names)
             (let ((recursion-scope (map
                                     (lambda (def-name)
                                       (let ((def (hash-table-ref defs def-name)))
                                         (assert (function-definition? def))
                                         (cons def-name (make-function-type
                                                         (map (lambda (_) (new-var))
                                                              (function-definition-arguments def))
                                                         (new-var)
                                                         #f
                                                         #f))))
                                     def-names)))
               (for-each
                (lambda (def-name)
                  (let ((subs (annotate-definition def-name
                                                   (cons recursion-scope toplevel-scope)
                                                   (cdr (assoc def-name recursion-scope)))))
                    (when subs
                      (set! recursion-scope (map-cdr (lambda (x) (apply-substitutions subs x))
                                                     recursion-scope)))))
                def-names))
             (annotate-definition def-names toplevel-scope #f)))
       rec-groups)))

  (define (pass-items-in-dependency-order pass mod)
    (match-let ([(_ . dep-groups) (rmodule-call-graph mod)]
                [defs (rmodule-definitions mod)])
      (for-each
       (lambda (def-names)
         (unless (list? def-names)
           (set! def-names (list def-names)))
         (for-each
          (lambda (def-name)
            (let* ((def (hash-table-ref defs def-name)))
              (when (definition-body def)
                (enter-module-context mod def-name)
                (pass (definition-body def) def))))
          def-names))
       dep-groups)))

  (define (infer-type-variable-linearity mod)
    ;; This sub-pass goes through, and infers the possible linearity of all type variables
    ;; in the given expression. Then it updates the type of all those type-variables to
    ;; reflect their linearity.
    ;;
    ;; We keep track of all "tainted" type variables, which can't be linearity
    ;; We also keep track of the linearity of original type variables from polymorphic
    ;; bindings, so that we can propegate the tainting to the instanciated type variables
    (define-pass infer-type-variable-linearity
      : L5 (e def) -> * ()
      (definitions
        (define (use usages x ty)
          (if (type-variable? ty)
              (cons (list (token-value x) ty)
                    usages)
              usages))
        ;; Return a list of which type variables (based on their usage and taint) are
        ;; able to be linearr
        (define (linearity-of vars tys usages)
          (map (lambda (var ty)
                 (and (not (tainted? ty))
                      (= 1 (count (lambda (x) (string=? var (car x))) usages))))
               vars
               tys))
        (define tainted-variables '())
        (define original-poly-types (filter (lambda (x) x)
                                            (map (lambda (x)
                                                   (if (poly-type? (definition-type (cdr x)))
                                                       (cons (car x) (definition-type (cdr x)))
                                                       #f))
                                                 (hash-table->alist (rmodule-definitions mod)))))
        (define internal-bindings '(()))
        ;; If it's a poly type, then store it
        (define (bind-poly-type x ty)
          (when (poly-type? ty)
            (push-set! original-poly-types (cons (token-value x) ty))))
        (define (tainted? x)
          (member x tainted-variables))
        (define (resolve-linear? t)
          (linear? (if (type-name? t)
                       (get-module-type (rmodule-name mod)
                                        (type-name-name t))
                       t)))
        ;; Lookup a binding's type, if it was bound to a poly-type then go through
        ;; and taint any type variables that are instantiations of tainted type
        ;; variables from the original poly-type.
        (define (taint-poly-type x ty)
          (let ((org-ty (assoc (token-value x) original-poly-types)))
            (when org-ty
              (for-each (match-lambda [(inst . org)
                                       (cond [(and (type-variable? org)
                                                   (tainted? inst)
                                                   (not (tainted? org)))
                                              ;; (printf "Tainted because it's an instanciation of ~A: ~A\n" org inst)
                                              (push-set! tainted-variables org)]
                                             [(and (not (type-variable? org))
                                                   (resolve-linear? org)
                                                   (tainted? inst))
                                              (module-fail mod (list 'linearity-mismatch x inst org))])])
                        (unify-types (poly-type-inner-type (cdr org-ty)) ty)))))
        ;; Go trough the current local scope, and taint any non-linear type variables
        ;; then pop the scope.
        (define (taint-non-linear-in-scope! usages)
          (let-values ([(vars tys) (unzip2 (car internal-bindings))])
            (for-each (lambda (lin? v ty)
                        ;; Taint any type variables of binding that turned out not be linear
                        (when (and (not lin?)
                                   (not (tainted? ty)))
                          ;; (printf "Tainted because not linear: ~A (from ~A)\n" ty v)
                          (push-set! tainted-variables ty)))
                      (linearity-of vars tys usages)
                      vars
                      tys)
            ;; Remove any bindings from usages since they're not needed outside this scope
            (let ((usages* (lset-difference equal? usages (car internal-bindings))))
              ;; Pop the binding scope
              (set! internal-bindings (cdr internal-bindings))
              usages*)))
        ;; Taint any type variables captures non-linearily
        (define (taint-non-linear-captures! E ty)
          (let ((usages (taint-non-linear-in-scope! (Typed E))))
            (when (and (not (linear? ty))
                       (not (null? usages)))
              ;; (printf "Tainted because was captured non-linearily ~A\n" usages)
              (set! tainted-variables (lset-union equal? tainted-variables (map cadr usages))))
            usages))
        (define (scope-from x* t*)
          (filter (lambda (binding)
                    (type-variable? (cadr binding)))
                  (zip (map token-value x*) t*)))
        (define (add-to-scope! x t)
          (set! internal-bindings (cons (cons (list (token-value x) t)
                                              (car internal-bindings))
                                        (cdr internal-bindings)))))
      (Expr : Expr (e ty) -> * ()
            [,i '()]
            [,k '()]
            [,q '()]
            [,x (begin (taint-poly-type x ty)
                       (use '() x ty))]
            [,b '()]
            [(do ,l ,E* ...)
             ;; For all non-final expressions in the do expression, if they result
             ;; in a type variable, taint that type variable, since it means it's
             ;; thrown away, making it impossible to be a linear type
             ;; NOTE: We don't handle variables being used but thrown away like:
             ;;   (lambda (x) (do x 3))
             ;; Which registers as if x was used once, making x linear but since
             ;; this scenario always causes the type of x to be tainted it has the
             ;; same effect anyway.
             (begin
               ;; Introduce a new binding scope
               (push-set! internal-bindings '())
               ;; Find thrown-away types
               (for-each (lambda (x)
                           (when (and (type-variable? x)
                                      (not (tainted? x)))
                             ;; (printf "Tainted because dropped in do: ~A\n" x)
                             (push-set! tainted-variables x)))
                         (map type-of (cdr (reverse E*))))
               (let ((usages (fold-left (lambda (acc E)
                                          (append (Typed E) acc))
                                        '()
                                        E*)))
                 ;; Check for polymorphic linearity of bindings
                 (taint-non-linear-in-scope! usages)))]
            ;; Taint all type variables that are only used in one of the arms
            [(if ,l ,E0 ,E1 ,E2)
             (let ((E1-usages (Typed E1))
                   (E2-usages (Typed E2)))
               (for-each (lambda (x)
                           ;; (printf "Tainted because wasn't in both branches: ~A\n" x)
                           (push-set! tainted-variables (cadr x)))
                         (lset-xor equal? E1-usages E2-usages))
               (append (Typed E0)
                       E1-usages))]
            [(def ,l ,x ,t ,E)
             ;; Add the binding to the current scope
             (begin (add-to-scope! x t)
                    (bind-poly-type x t)
                    (Typed E))]
            [(def ,l (,x (,x* ,t*) ...) ,t ,E)
             ;; Add the binding to the current scope
             (begin (add-to-scope! x t)
                    (bind-poly-type x t)
                    ;; Bind any poly-types
                    (for-each bind-poly-type x* t*)
                    ;; Introduce bindings that have a polymorphic type
                    (push-set! internal-bindings (scope-from x* t*))
                    (taint-non-linear-captures! E ty))]
            ;; Bind any possible poly-types, then infer linearity of bindings
            [(let ,l ((,x* ,t* ,E*) ...) ,E)
             ;; Bind any poly-types
             (let ((E*-usages (begin (for-each bind-poly-type x* t*)
                                     (concatenate (map Typed E*)))))
               (push-set! internal-bindings (scope-from x* t*))
               (let ((usages (append (Typed E) E*-usages)))
                 (taint-non-linear-in-scope! usages)))]
            [(lambda ,l ,attrs ((,x* ,t*) ...) ,E)
             (begin
               ;; Bind any poly-types
               (for-each bind-poly-type x* t*)
               ;; Introduce bindings that have a polymorphic type
               (push-set! internal-bindings (scope-from x* t*))
               (taint-non-linear-captures! E ty))]

            [(destruct ,l ,E0 (,t (,x0* ,x1*) ...) ,E1)
             (let ((struct-ty (resolve mod t))
                   (E0-usages (Typed E0)))
               (push-set! internal-bindings (scope-from x0*
                                                        (map (lambda (f)
                                                               (type-of-field struct-ty (token-value f)))
                                                             x1*)))
               (let ((usages (append (Typed E1) E0-usages)))
                 (taint-non-linear-in-scope! usages)))]
            [(construct ,l ,t (,x* ,E*) ...)
             (concatenate (map Typed E*))]
            [(variant ,l ,t ,x ,E)
             (Typed E)]
            [(case ,l ,E0 ,t ((,x0 ,x1) ,E*) ...)
             (let* ((E0-usages (Typed E0))
                    (sum-ty (resolve mod t))
                    (variants (sum-type-variants sum-ty))
                    (arms (map (lambda (x t E)
                                 (push-set! internal-bindings (scope-from (list x) (list t)))
                                 (taint-non-linear-in-scope! (Typed E)))
                               x1 (map (lambda (x) (cdr (assoc (token-value x) variants))) x0) E*))
                    (first-arm (car arms)))
               (for-each (lambda (x)
                           ;; (printf "tainted because wasn't in all arms:~a\n" x)
                           (push-set! tainted-variables (cadr x)))
                         (fold-left (lambda (acc arm)
                                      (lset-union equal? (lset-xor equal? first-arm arm) acc))
                                    '()
                                    (cdr arms)))
               (append E0-usages
                       first-arm))]
            [(,E ,E* ...)
             ;; Make sure we gather usages before (so we also have any taintings ready)
             (let ((usages (concatenate (map Typed (cons E E*)))))
               (for-each (lambda (ty E)
                           (when (and (type-variable? (type-of E))
                                      (not (tainted? (type-of E)))
                                      (tainted? ty))
                             ;; (printf "tainted because is argument to a non-linear parameter: ~a" (type-of e))
                             (push-set! tainted-variables (type-of E))))
                         (function-type-argument-types (type-of E))
                         E*)
               (when (token? (expr-of E))
                 (let ((orig-fty (assoc (token-value (expr-of E)) original-poly-types)))
                   (when orig-fty
                     (unless (instance-of? (type-of E) (cdr orig-fty))
                       (module-fail mod (list 'linearity-mismatch e (type-of E) (cdr orig-fty)))))))
               usages)])
      (Typed : Typed (E) -> * ()
             [(: ,e ,t) (Expr e t)]
             [,i (errorf 'linearity-check "Encountered invalid AST node")])

      ;; (printf "\n")
      (let* ((usages (Typed e))
             ;; If a variable's polymorphic type ends up being a possibly linear type
             ;; then we have to taint it, since toplevel variables with linear values
             ;; doesn't make much sense
             (_ (when (and (type-variable? (type-of e))
                           (not (tainted? (type-of e)))
                           (variable-definition? def))
                  (push-set! tainted-variables (type-of e))))
             (all-variables (free-variables-in-expr e))
             (linear-variables (lset-difference equal? all-variables tainted-variables))
             (lin-subs (map* (lambda (var) (cons var (new-linear-var))) linear-variables)))
        ;; (printf "Linearizing: ~A\n" linear-variables)
        ;; (printf "~A\n" (strip-syntax (unparse-L5 e) L5-language-constructs))
        (definition-body-set! def (substitute-types (definition-body def) lin-subs))
        (definition-type-set! def (apply-substitutions lin-subs (definition-type def)))
        ;; (printf "\n")
        usages))

    (pass-items-in-dependency-order
     infer-type-variable-linearity
     mod))

  (define (linearity-check mod)
    (define-pass linearity-check
      : L5 (e def) -> * ()
      (definitions
        (define (unimpl)
          (errorf #f "Not implemented: '~A'" e))
        (define (resolve-linear? t)
          (let ((rty (if (type-name? t)
                         (type-application (get-module-type (rmodule-name mod) (type-name-name t))
                                           (type-name-arguments t))
                         t)))
            (match rty
              [($ poly-type args inner) (resolve-linear? inner)]
              [($ struct-type :_ field-tys ) (or (linear? rty)
                                                 (any resolve-linear? field-tys))]
              [($ sum-type variants) (or (linear? rty)
                                         (any (compose resolve-linear? cdr) variants))]
              [:_ (linear? rty)])))
        (define (intro usages E)
          (let* ((p? (pair? E))
                 (ty (if p? (cdr E) (type-of E)))
                 (ex (if p? (car E) (expr-of E))))
            (if (resolve-linear? ty)
                (cons `(intro ,ex)
                      usages)
                usages)))
        (define (intro-bound usages x ty)
          (if (resolve-linear? ty)
              (cons `(intro-bound ,(token-value x))
                    usages)
              usages))
        (define (use usages E)
          (if (resolve-linear? (type-of E))
              (remove-first* `(intro ,(expr-of E)) usages
                             (lambda ()
                               (module-fail mod `(overused-linear-value value ,(expr-of E)))
                               usages))
              usages))
        (define (check-bound usages var ty)
          (let ((the-use `(use-bound ,(token-value var))))
            (if (resolve-linear? ty)
                (let ((remaining (remove-first* the-use usages
                                                (lambda ()
                                                  (module-fail mod `(unused-linear-value
                                                                     binding ,(token-value var)))
                                                  usages))))
                  (if (member the-use remaining)
                      (begin (module-fail mod `(overused-linear-value binding ,(token-value var)))
                             ;; Remove it to make it not trigger non-linear-lambda-captures
                             (remove the-use remaining))
                      remaining))
                usages)))
        (define (closure x* t* E ty)
          (let* ((usages (-> (Typed E)
                             (use E)))
                 (usages*
                  (fold-left check-bound usages x* t*)))
            (if (and (not (null? usages*))
                     (not (linear? ty)))
                (begin (module-fail mod (list 'non-linear-closure e usages*))
                       '())
                usages*))))
      (Expr : Expr (e ty) -> * ()
            [,i '()]
            [,k '()]
            [,q '()]
            ;; Use the named binding if it's linear and then introduce its value
            [,x (if (resolve-linear? ty)
                    `((use-bound ,(token-value x)) (intro ,x))
                    '())]
            [,b '()]
            ;; Do expressions inherit all their sub-expression's usages, but only
            ;; use (and reintroduce) the last sub-exression's usages
            [(do ,l ,E* ...)
             (let next ((E* E*)
                        (usages* '()))
               (match E*
                 [() '()]
                 [(last-E) (let next ((final-usages '())
                                      (usages (-> (Typed last-E)
                                                  (use last-E)
                                                  (append (intro usages* (cons e ty))))))
                             ;; Report unused bindings
                             (match usages
                               [() final-usages]
                               [(('intro-bound x) usages ...)
                                (begin (module-fail mod `(unused-linear-value binding ,x))
                                       final-usages)]
                               ;; Try to consume the usage if it was introduced in the do
                               [(('use-bound x) usages ...)
                                (let ((usages* (remove-first* `(intro-bound ,x) usages (lambda ()
                                                                                         #f))))
                                  (if usages*
                                      (next final-usages usages*)
                                      ;; Otherwise let the usage bubble up
                                      (next (cons `(use-bound ,x) final-usages) usages)))]
                               [(usage usages ...)
                                (next (cons usage final-usages) usages)]))]
                 [(E E* ...) (let ((E-usages (Typed E)))
                               ;; Check if a linear value was introduced in this sub-expression
                               (if (member `(intro ,(expr-of E)) E-usages)
                                   ;; Since it's last, we fail right away
                                   (begin (module-fail mod `(unused-linear-value value ,(expr-of E)))
                                          usages*)
                                   (next E* (append E-usages usages*))))]))]
            ;; The condition is always used (if linear), but both arms must match in usages
            ;; otherwise that's an error. The entire if-expr then has the usages of the
            ;; condition expression and an arm
            [(if ,l ,E0 ,E1 ,E2)
             (let ((E0-usages (-> (Typed E0)
                                  (use E0)))
                   (E1-usages (-> (Typed E1)
                                  (use E1)))
                   (E2-usages (-> (Typed E2)
                                  (use E2))))
               (-> (if (not (null? (lset-xor equal? E1-usages E2-usages)))
                       (begin (module-fail mod (list 'mismatching-usages
                                                     (list E1-usages E2-usages)
                                                     (list E1 E2)))
                              E0-usages)
                       (append E0-usages E1-usages))
                   (intro (cons e ty))))]
            ;; Inherit from the body and use it, introduce the name
            [(def ,l ,x ,t ,E)
             (-> (Typed E)
                 (use E)
                 (intro-bound x t))]
            ;; See lambda (introduces binding if linear function)
            [(def ,l (,x (,x* ,t*) ...) ,t ,E)
             (-> (closure x* t* E ty)
                 (intro-bound x t))]
            ;; Inherits all the usages from binding values, introduces any linear
            ;; bound values, inherits and uses the body, checks if all linear bound
            ;; values have been used.
            [(let ,l ((,x* ,t* ,E*) ...) ,E)
             (let ((usages (->> E*
                                (map (lambda (x) (use (Typed x) x)))
                                (fold-left append (Typed E)))))
               (-> (fold-left check-bound usages x* t*)
                   (use E)
                   (intro (cons e ty))))]
            ;; Make sure that all linear parameters are used in the body
            ;; If a lambda has "free" usages, i.e. it captures a linear value, then
            ;; the lambda itself must be linear, otherwise mutliple invocations of
            ;; the lambda would break linearity
            [(lambda ,l ,attrs ((,x* ,t*) ...) ,E)
             (-> (closure x* t* E ty)
                 (intro (cons e ty)))]
            ;; Destruction uses the destructee if it's linear (and any linear values used in the body)
            ;; and possibly introduces a new linear value if the body expression is a linear type
            [(destruct ,l ,E0 (,t (,x0* ,x1*) ...) ,E1)
             (let* ((struct-ty (resolve mod t))
                    (E0-usages (Typed E0))
                    (E1-usages (fold-left (lambda (usages x ty)
                                            (check-bound usages x ty))
                                          (Typed E1)
                                          x0*
                                          (map (lambda (f) (type-of-field struct-ty (token-value f))) x1*))))
               (-> (append E0-usages E1-usages)
                   (intro (cons e ty))
                   (use E0)
                   (use E1)))]
            ;; Construction uses any linear values used in the construction expressions, and possibly
            ;; introduces a linear value if the constructed type is linear
            [(construct ,l ,t (,x* ,E*) ...)
             (fold-left (lambda (usages E)
                          (-> usages
                              (append (Typed E))
                              (use E)))
                        (intro '() (cons e ty))
                        E*)]
            ;; Inherit and use any linear values from the body, and possibly introduce a linear value
            [(variant ,l ,t ,x ,E)
             (-> (Typed E)
                 (use E)
                 (intro (cons e ty)))]
            ;; Inherit and use any usages from the cased value. Introduce any linear variants,
            ;; all arms must match in usages possibly introduce a linear value and inherit
            ;; from an arm too
            [(case ,l ,E0 ,t ((,x0 ,x1) ,E*) ...)
             (let* ((sumTy (resolve mod t))
                    (E0-usages (Typed E0))
                    (E*-usages (map (lambda (v x E)
                                      (-> (Typed E)
                                          (check-bound x (cdr (assoc (token-value v)
                                                                     (sum-type-variants sumTy))))
                                          (use E)))
                                    x0 x1 E*))
                    (first-arm (car E*-usages)))
               (if (every (lambda (x) (null? (lset-xor equal? first-arm x))) (cdr E*-usages))
                   (-> E0-usages
                       (use E0)
                       (append first-arm)
                       (intro (cons e ty)))
                   (begin (module-fail mod (list 'mismatching-usages
                                                 E*-usages
                                                 E*))
                          E0-usages)))]
            ;; Application uses any of it's direct arguments if they're linear, inherits they
            ;; linear usages and possibly introduces a new linear value if the return type is linear
            [(,E ,E* ...)
             (let ((E-usages (Typed E))
                   (E*-usages (map Typed E*)))
               (for-each (lambda (ty arg Earg)
                           (when (and (resolve-linear? arg)
                                      (not (resolve-linear? ty)))
                             (module-fail mod (list 'linearity-mismatch E Earg))))
                         (function-type-argument-types (type-of E))
                         (map type-of E*)
                         E*)
               (fold-left (lambda (usages E E-usages)
                            (append
                             (use E-usages E)
                             usages))
                          (intro '() (cons e ty))
                          (cons E E*)
                          (cons E-usages E*-usages)))])
      (Typed : Typed (E) -> * ()
             [(: ,e ,t) (Expr e t)]
             [,i (errorf 'linearity-check "Encountered invalid AST node")])

      (let ((usages
             (if (function-definition? def)
                 (let ((f-ty (definition-type def)))
                   (closure (function-definition-arguments def)
                            (function-type-argument-types (if (poly-type? f-ty)
                                                              (poly-type-inner-type f-ty)
                                                              f-ty))
                            e
                            (definition-type def)))
                 (Typed e))))
        (unless (null? usages)
          (module-fail mod (list 'linearity-invariant-broken usages)))
        #f))

    (pass-items-in-dependency-order
     linearity-check
     mod))

  (register-pass 'pre-pass
                 #f
                 #f
                 'L1
                 #f)

  (register-pass 'check-attributes
                 check-attributes
                 'L1
                 'L2
                 'item)

  (register-pass 'gather-toplevel-expressions
                 gather-toplevel-expressions
                 'L2
                 'L2
                 'module)

  (register-pass 'flatten-dos
                 flatten-dos
                 'L2
                 'L2
                 'item)

  (register-pass 'lower-type-constructors
                 lower-type-constructors
                 'L2
                 'L3
                 'item)

  (register-pass 'make-variables-unique
                 make-variables-unique
                 'L3
                 'L3
                 'item)

  (register-pass 'lower-match
                 lower-match
                 'L3
                 'L4
                 'item)

  (register-pass 'analyze-call-graph
                 analyze-call-graph
                 'L4
                 'L4
                 'module)

  (register-pass 'annotate-types
                 annotate-types
                 'L4
                 'L5
                 'module)

  (register-pass 'infer-type-variable-linearity
                 infer-type-variable-linearity
                 'L5
                 'L5
                 'module)

  (register-pass 'linearity-check
                 linearity-check
                 'L5
                 'L5
                 'module))
