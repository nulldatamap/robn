;; -*- chezscheme -*-
#!chezscheme
(library (compiler pipeline)
  (export make-pipeline-config
          pipeline-config?
          pipeline-config-target-pass

          make-pass-info
          pass-info?
          pass-info-name
          pass-info-pass
          pass-info-inlang
          pass-info-outlang
          pass-info-kind

          passes
          register-pass
          lookup-pass
          last-pass
          construct-pass-pipeline)
  (import (chezscheme)
          (compiler languages)
          (util misc))

  (define-record-type pipeline-config
    (fields target-pass))

  (define-record-type pass-info
    (fields name
            pass
            inlang
            outlang
            kind))

  (define passes (make-hashtable symbol-hash symbol=?))

  (define (register-pass name pass inlang outlang kind)
    (hashtable-set! passes name
                    (make-pass-info name
                                    pass
                                    (and inlang (lookup-language inlang))
                                    (and outlang (lookup-language outlang))
                                    kind)))

  (define (lookup-pass name)
    (or (hashtable-ref passes name #f)
        (errorf 'lookup-pass "No pass named '~A', did you forget to register it?" name)))

  (define (last-pass config)
    (let ((pipeline (construct-pass-pipeline config)))
      (car (reverse pipeline))))

  (define (construct-pass-pipeline config)
    (define whole-pipeline '(gather-toplevel-expressions
                             flatten-dos
                             lower-type-constructors
                             make-variables-unique
                             lower-match
                             analyze-call-graph
                             annotate-types
                             infer-type-variable-linearity
                             linearity-check))
    (let ((target-pass (pipeline-config-target-pass config)))
      (if (eq? target-pass 'pre-pass)
          '()
          (map lookup-pass
               (if (eq? target-pass 'last)
                   whole-pipeline
                   (let next ((remaining whole-pipeline))
                     (cond
                      [(null? remaining) (errorf 'construct-pass-pipeline "Unknown compiler pipeline/stage: ~A" target-pass)]
                      [(eq? (car remaining) target-pass) (list target-pass)]
                      [else (cons (car remaining) (next (cdr remaining)))])))))))

  (record-writer (type-descriptor pass-info)
                 (lambda (r p wr)
                   (display "#pass(" p)
                   (wr (pass-info-name r) p)
                   (display " " p)
                   (wr (or (pass-info-inlang r) "*") p)
                   (display " -> " p)
                   (wr (or (pass-info-outlang r) "*") p)
                   (display ")" p))))
