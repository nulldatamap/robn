#!chezscheme
(library (util syntax-helpers)
  (export strip-syntax
          unstrip-syntax
          strip-type-syntax
          unstrip-type-syntax)
  (import (except (chezscheme)
                  compile-file)
          (syntax combinators)
          (syntax token)
          (compiler core)
          (compiler types)
          (util arrows)
          (only (srfi s1 lists)
                take-while drop-while)
          (matchable))

  (define (strip-syntax x language-constructs)
    (define (strip-syntax* x) (strip-syntax x language-constructs))
    (cond
     [(type? x) (list ': (strip-type-syntax x))]
     [(token? x) (case (token-kind x)
                   ;; TODO: string escape
                   [(string) (string-append "\"" (token-value x) "\"")]
                   [(keyword) (string->symbol (string-append ":" (token-value x)))]
                   [(ident) (string->symbol (token-value x))]
                   [(qualified-symbol) (let ((q (token-value x)))
                                         (cons ':: (append (map string->symbol (qualified-symbol-namespace q))
                                                           (list (string->symbol (qualified-symbol-identifier q))))))]
                   [else (token-value x)])]
     [(list? x) (let ((x (match x
                           [('lambda :_ ((? (lambda (x)
                                              (token-kind? x 'keyword)) kws) ...) rest ...)
                            (append '(lambda #f)
                                    kws
                                    rest)]
                           [a a])))(cond
                 ;; If it has a keyword source location, then remove it
                 [(and (> (length x) 1)
                       (member (car x) language-constructs))
                  (map strip-syntax* (cons (car x) (cddr x)))]
                 ;; If it has a type annotation, strip the type record
                 ;; TODO:
                 [(and (= (length x) 3)
                       (eq? ': (car x)))
                  (list ':
                        (strip-syntax* (cadr x))
                        (strip-type-syntax (caddr x)))]
                 [else (map strip-syntax* x)]))]
     [else x]))

  (define (unstrip-syntax x language-constructs)
    (define (unstrip-syntax* x) (unstrip-syntax x language-constructs))
    (cond
     [(list? x) (-> x
                    (match
                      [`(: ,ty) (unstrip-type-syntax ty)]
                      [`(: ,v ,ty) (list ':
                                         (unstrip-syntax* v)
                                         (unstrip-type-syntax ty))]
                      [(':: rest ...) (make-token 'qualified-symbol (list->qualified-symbol (map symbol->string rest)) #f #f)]
                      [((a ...) b ...) (cons (unstrip-syntax* a)
                                             (map unstrip-syntax* b))]
                      [(a b ...) (if (member a language-constructs)
                                     (cons a (cons #f (map unstrip-syntax* b)))
                                     (cons (unstrip-syntax* a)
                                           (map unstrip-syntax* b)))]
                      [_ x])
                    (match
                        [('lambda #f rest ...)
                         (append '(lambda #f)
                                 (cons (take-while (lambda (x)
                                                     (or (token-kind? x 'keyword)
                                                         (string? x)))
                                                   rest)
                                       (drop-while (lambda (x)
                                                     (or (token-kind? x 'keyword)
                                                         (string? x)))
                                                   rest)))]
                      [a a]))]
     [(symbol? x) (let ((s (symbol->string x)))
                    (cond
                     [(eq? (string-ref s 0) #\!)
                      (let ((v (substring s 1 (string-length s))))
                        (if (eq? (string-ref v 0) #\:)
                            (substring v 1 (string-length v))
                            (string->symbol v)))]
                     [(eq? (string-ref s 0) #\:)
                      (make-token 'keyword (substring s 1 (string-length s)) #f #f)]
                     [else (make-token 'ident s #f #f)]))]
     [(string? x) (make-token 'string x #f #f)]
     [(number? x) (make-token 'number x #f #f)]
     [else x]))

  (define (strip-type-syntax x)
    (match x
      [($ builtin-type bty) (string->symbol bty)]
      [($ type-name ty args) (if (null? args)
                                 (string->symbol ty)
                                 (cons (string->symbol ty)
                                       (map strip-type-syntax args)))]
      [($ function-type args ret lin?) (list (if lin? '-o '->)
                                             (map strip-type-syntax args)
                                             (strip-type-syntax ret))]
      [($ poly-type tyvars inner)
       (list 'forall
             (map strip-type-syntax tyvars)
             (strip-type-syntax inner))]
      [($ struct-type fields types)
       (cons '<anonymous-struct>
             (map list fields types))]
      [($ type-variable var lin?)
       `(quote ,(string->symbol (if lin? (string-append "?" var) var)))]))

  (define (unstrip-type-syntax x)
    (match x
      [`(quote ,v) (let ((name (symbol->string v)))
                     (if (eq? (string-ref name 0) #\?)
                         (make-type-variable (substring name 1 (string-length name)) #t #f)
                         (make-type-variable name #f #f)))]
      [`(-> ,(args ...) ,ret) (make-function-type
                               (map unstrip-type-syntax args)
                               (unstrip-type-syntax ret)
                               #f #f)]
      [`(-o ,(args ...) ,ret) (make-function-type
                               (map unstrip-type-syntax args)
                               (unstrip-type-syntax ret)
                               #t #f)]
      [`(forall ,(vars ...) ,inner)
       (make-poly-type
        (map unstrip-type-syntax vars)
        (unstrip-type-syntax inner)
        #f)]
      [(n args ...) (make-type-name (symbol->string n) (map unstrip-type-syntax args) #f)]
      [else (if (symbol? x)
                (let* ((ty (symbol->string x))
                       (bty (hashtable-ref builtin-types ty #f)))
                  (or bty (make-type-name ty '() #f)))
                (errorf 'unstrip-type-syntax "Invalid type syntax: ~A\n" x))])))
