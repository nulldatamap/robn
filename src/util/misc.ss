
(library (util misc)
  (export symbol-concat
          lapply-set!
          rapply-set!
          push-set!
          try*
          try
          find-duplicate
          compare-sets
          map-car
          map-cdr
          compose
          compose*
          remove-first
          remove-first*
          true?
          map*
          map-values2
          hashtable-for-each
          hashtable-fold)
  (import (chezscheme)
          (only (srfi s43 vectors)
                vector-fold)
          (only (srfi s1 lists)
                lset-intersection))

  (define-syntax symbol-concat
    (lambda (x)
      (syntax-case x ()
        [(_ a b) (datum->syntax #'symbol-concat
                                (string->symbol
                                 (string-append (symbol->string (datum a))
                                                (symbol->string (datum b)))))])))

  (define-syntax lapply-set!
    (syntax-rules ()
      [(_ var-name f args ...)
       (set! var-name (f var-name args ...))]))

  (define-syntax rapply-set!
    (syntax-rules ()
      [(_ var-name f args ...)
       (set! var-name (f args ... var-name))]))

  (define-syntax push-set!
    (syntax-rules ()
      [(_ var-name value)
       (rapply-set! var-name cons value)]))

  (define-syntax try*
    (syntax-rules ()
      [(_ (x call) body ...)
       (let-values ([(x errs) call])
         (if (or (null? errs) (not errs))
             (values (begin x body ...) #f)
             (values #f errs)))]))

  (define-syntax try
    (syntax-rules ()
      [(_ (x call) body ...)
       (let-values ([(x errs) call])
         (if (or (null? errs) (not errs))
             (begin (values x #f) body ...)
             (values #f errs)))]))

  ;; Finds the first duplicate
  (define (find-duplicate xs key)
    (let next ((visited '())
               (rest xs))
      (if (null? rest)
          #f
          (let ((keyd (key (car rest))))
            (if (member keyd visited)
              (car rest)
              (next (cons keyd visited)
                    (cdr rest)))))))

  ;; Return two values, only-a and only-b
  ;; These value contain all values from their respective set which are not
  ;; found in the intersection between the two input sets.
  (define (compare-sets cmp? a b)
    (let ((intersection (lset-intersection cmp? a b))
          (only-a '())
          (only-b '()))
      (for-each (lambda (x)
                  (unless (member x intersection)
                    (push-set! only-a x)))
                a)
      (for-each (lambda (x)
                  (unless (member x intersection)
                    (push-set! only-b x)))
                b)
      (values only-a only-b)))

  (define-syntax compose
    (syntax-rules ()
      [(_ f g)
       (lambda (x) (f (g x)))]
      [(_ f g h ...)
       (compose f (compose g h ...))]))

  (define (compose* f g)
    (lambda x (f (apply g x))))

  ;; Apply f on the car of each element in l
  (define (map-car f l)
    (map (lambda (x) (cons (f (car x)) (cdr x))) l))

  ;; Apply f on the cdr of each element in l
  (define (map-cdr f l)
    (map (lambda (x) (cons (car x) (f (cdr x)))) l))

  ;; Ordered map
  (define (map* f . xs)
    (let ((xs xs))
      (if (null? (car xs))
          '()
          (cons (apply f (map car xs))
                (apply map* f (map cdr xs))))))

  ;; Maps over a single list with a function that returns two values
  ;; returns two resulting lists
  (define (map-values2 f xs)
    (if (null? xs)
        (values '() '())
        (let-values ([(a  b)  (f (car xs))]
                     [(as bs) (map-values2 f (cdr xs))])
          (values (cons a as)
                  (cons b bs)))))

  (define (remove-first* elm xs not-found)
    (call/cc
     (lambda (k)
       (let next ((l xs))
         (cond
          [(null? l)
           (k (not-found))]
          [(equal? elm (car l)) (cdr l)]
          [else (cons (car l) (next (cdr l)))])))))

  (define (remove-first elm xs)
    (remove-first* elm xs
                   (lambda ()
                     (errorf 'remove-first "Element ~A was not present in ~A" elm xs))))

  (define (true? x)
    (if x #t #f))

  (define (hashtable-for-each t f)
    (let-values (((keys entries) (hashtable-entries t)))
      (vector-for-each f keys entries)))

  (define (hashtable-fold f x h)
    (let-values ([(keys vals) (hashtable-entries h)])
      (vector-fold (lambda (i acc k v) (f acc k v)) x keys vals))))
