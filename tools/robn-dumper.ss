#!/usr/bin/env scheme --libdirs .:./src:./libs/nanopass:./libs/thunderchez --script
(import (except (chezscheme)
                compile-file)
        (srfi s37 args-fold)
        (syntax combinators)
        (util arrows)
        (compiler languages)
        (compiler passes)
        (compiler pipeline)
        (compiler modules)
        (compiler robn)
        (util syntax-helpers))

(define (set-pass settings pass)
  (make-compiler-settings (string->symbol pass)))

(define options
  (list (option '(#\p "pass") #f #f
                (lambda (option name pass settings files)
                  (values (set-pass settings pass) files)))))

(define (compile-and-dump f settings)
  (printf "~A:\n" f)
  (let-values ([(x errors) (compile-file f settings)]
               [(outlang) (-> settings
                              (compiler-settings-pipeline-config)
                              (last-pass)
                              (pass-info-outlang))])
    (if errors
        (begin (printf "Failed to compile: ~A\n" f)
               (for-each (lambda (x) (printf "~A\n" x))
                         (if (list? errors) errors (list errors))))
        (for-each (lambda (x)
                    (printf ";; (module ~A)\n" (car x))
                    (for-each (lambda (y) (printf "~A\n"
                                                  (->> outlang
                                                       (language-info-language-constructs )
                                                       (strip-syntax y))))
                              (cdr x))
                    (printf "\n"))
                  (-> outlang
                      (language-info-unparse)
                      (dump-modules))))))


(let-values (([settings files]
              (args-fold (command-line-arguments)
                         options
                         (lambda (option name arg settings files)
                           (errorf #f "Unknown option: ~A" name))
                         (lambda (f settings files)
                           (values settings (cons f files)))
                         default-settings
                         '())))
  (for-each (lambda (f)
              (compile-and-dump f settings))
            (reverse files)))
